let peopleData = require('./people_data')

//1. Make the balance into a number. If the number is invalid, default to 0. Do not overwrite the existing key, create a new one instead.

function balanceIntoNumber(peopleData) {
    let result = peopleData.map((people) => {
        let balance = Number((people.balance.slice(1)).replaceAll(",", ""));

        if (isNaN(balance)) {
            people["balanceInNumber"] = 0;
        } else {
            people["balanceInNumber"] = balance;
        }

        return people;
    })

    return result;
}



//2. Sort by descending order of age.

function sortAgeInDecending(peopleData) {
    let result = balanceIntoNumber(peopleData).sort((first, second) => {
        return second.age - first.age;
    })

    return result;
}


//3. Flatten the friends key into a basic array of names.
function friendsName(peopleData) {
    let result = sortAgeInDecending(peopleData).map((people) => {
        let friends = people.friends;

        let name = friends.map((friend) => {
            return friend.name;
        })

        people["friends"] = name;
        return people;
    })

    return result;
}
// console.log(friendsName(peopleData));

//4. Remove the tags key
function removeTagsKey(peopleData) {
    let result = friendsName(peopleData).map((people) => {
        delete people.tags;
        return people;
    })

    return result;
}



//5. Make the registered date into the DD/MM/YYYY format.

function formattingDate(peopleData) {
    let result = removeTagsKey(peopleData).map((people) => {
        let registerDate = (people.registered.slice(0, 10)).split("-");

        registerDate = registerDate[2] + "/" + registerDate[1] + "/" + registerDate[0];

        people["registerDate"] = registerDate;
        return people;
    })

    return result
}


//6. Filter only the active users.
function activeUser(peopleData) {
    let result = formattingDate(peopleData).filter((people) => {
        return people.isActive
    })

    return result;
}



//7. Calculate the total balance available.
function totalBalance(peopleData) {
    let result = activeUser(peopleData).reduce((sum, people) => {
        if (people.balanceInNumber > 0) {
            sum += people.balanceInNumber;
        }
        return sum;
    }, 0)

    return result;
}
